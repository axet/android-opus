#include <jni.h>
#include <stdlib.h>
#include <stdio.h>
#include <opus.h>

typedef struct {
    OpusEncoder *encoder;
    int channels;
} opus_t;

JNIEXPORT void JNICALL
Java_com_github_axet_opusjni_Opus_open(JNIEnv *env, jobject thiz, jint channels,
                                        jint sampleFreq, jint brate) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");

    int err = -1;
    opus_t *opus = malloc(sizeof(opus_t));
    opus->encoder = opus_encoder_create(sampleFreq, channels, OPUS_APPLICATION_AUDIO, &err);
    opus->channels = channels;
    (*env)->SetLongField(env, thiz, fid, (jlong) opus);

    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }

    err = opus_encoder_ctl(opus->encoder, OPUS_SET_BITRATE(brate));

    if (err < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(err));
        return;
    }

    return;
}

JNIEXPORT jbyteArray JNICALL
Java_com_github_axet_opusjni_Opus_encode(JNIEnv *env, jobject thiz, jshortArray array, jint pos, jint len) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t*) (*env)->GetLongField(env, thiz, fid);

    char *out = 0;
    int outlen = 0;

    outlen = sizeof(short int) * len;
    if (outlen < 4096)
      outlen = 4096;
    out = malloc(outlen);

    jshort *bufferPtr = (*env)->GetShortArrayElements(env, array, NULL);
    int frameSize = len / opus->channels;
    outlen = opus_encode(opus->encoder, bufferPtr + pos, frameSize, out, outlen);
    (*env)->ReleaseShortArrayElements(env, array, bufferPtr, 0);
    
    if (outlen < 0) {
      jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
      (*env)->ThrowNew(env, class_rex, opus_strerror(outlen));
      free(out);
      return NULL;
    }

    jbyteArray ret = (*env)->NewByteArray(env, outlen);
    (*env)->SetByteArrayRegion(env, ret, 0, outlen, out);
    free(out);
    return ret;
}

JNIEXPORT jbyteArray JNICALL
Java_com_github_axet_opusjni_Opus_encode_1float(JNIEnv *env, jobject thiz, jfloatArray buf, jint pos, jint len) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t*) (*env)->GetLongField(env, thiz, fid);

    char *out = 0;
    int outlen = 0;

    outlen = sizeof(float) * len;
    if (outlen < 4096)
        outlen = 4096;
    out = malloc(outlen);

    jfloat *bufferPtr = (*env)->GetFloatArrayElements(env, buf, NULL);
    int frameSize = len / opus->channels;
    outlen = opus_encode_float(opus->encoder, bufferPtr + pos, frameSize, out, outlen);
    (*env)->ReleaseFloatArrayElements(env, buf, bufferPtr, 0);

    if (outlen < 0) {
        jclass class_rex = (*env)->FindClass(env, "java/lang/RuntimeException");
        (*env)->ThrowNew(env, class_rex, opus_strerror(outlen));
        free(out);
        return NULL;
    }

    jbyteArray ret = (*env)->NewByteArray(env, outlen);
    (*env)->SetByteArrayRegion(env, ret, 0, outlen, out);
    free(out);
    return ret;
}

JNIEXPORT void JNICALL
Java_com_github_axet_opusjni_Opus_close(JNIEnv *env, jobject thiz) {
    jclass cls = (*env)->GetObjectClass(env, thiz);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "J");
    opus_t *opus = (opus_t*) (*env)->GetLongField(env, thiz, fid);

    opus_encoder_destroy(opus->encoder);
    free(opus);

    (*env)->SetLongField(env, thiz, fid, 0);
    return;
}
