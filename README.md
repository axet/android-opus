# libopus for Android

Library to encode samples to opus.

# Natives

Only recent (19+) phones has no issue loading natives. Old phones (16+) unable to locate library dependencies. Very old phones (15+) unable choice proper native library ABI version and crash with error UnsatifiedLinkExcetpion.

  * https://gitlab.com/axet/android-audio-library/blob/master/src/main/java/com/github/axet/audiolibrary/encoders/FormatOPUS.java

```java
    import com.github.axet.androidlibrary.app.Native;

    Native.loadLibraries(context, new String[]{"opus", "opusjni"})
```

  * https://gitlab.com/axet/android-library/blob/master/src/main/java/com/github/axet/androidlibrary/app/Native.java


```gradle
dependencies {
    compile 'com.github.axet:opus:1.0.4'
}
```

# Compile

    # gradle clean
    # gradle :libopus:publishToMavenLocal publishToMavenLocal
    # gradle :libopus:publish publish
